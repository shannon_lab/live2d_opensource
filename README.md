Live2Dの優子ちゃんと脳クラゲ
====

## 説明

Live2Dの優子ちゃんと脳クラゲのLive2DのJavaScriptをオープンソース化したものです。 

## ファイル説明

### nokurage

このフォルダは脳クラゲのフォルダです。

### yukochan

このフォルダは優子ちゃんのフォルダです。

## 使用方法

### 口パクのさせ方。

以下の5種類のメソッド
```
playTalk01Motion()
playTalk02Motion()
playTalk03Motion()
playTalk04Motion()
playTalk05Motion()
```
で呼べます。

### 口パクの止め方。

```
playIdleMotion()
```
で呼べます。

## Contribution

Shannon Lab株式会社

## ライセンス

未定

## Author

[Shannon Lab](https://bitbucket.org/shannondev)
jmindview@gmail.comまでご連絡下さい。

